# Bonus.ly
A simple application for viewing Bonusly data via the Bonusly API.

This project was made with Javascript, jQuery, HTML and CSS.

## Quickstart
1. Follow this link: [https://lrisberg-bonusly.surge.sh/](https://lrisberg-bonusly.surge.sh/)
1. Enter your Bonusly authorization token
1. Click "Authorize!"

## Local Development
1. Clone this repository
1. Open `index.html` in your browser
