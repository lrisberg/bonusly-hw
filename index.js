$(document).ready(function() {
  function renderLogin() {
    const container = $("<div>").addClass("container login-container");
    const row = $("<div>").addClass("row");
    const login = $("<div>").addClass("login col-xs-6 col-xs-offset-3");
    const inputGroup = $("<div>").addClass("input-group");
    const input = $("<input>").addClass("form-control token-input").attr("type", "text").attr("placeholder", "Hello Aaron, enter your Bonusly authorization token...");
    const inputGroupButton = $("<span>").addClass("input-group-btn");
    const button = $("<button>").addClass("btn btn-default authorize").attr("type", "button").text("Authorize!");
    $("#main").append(container)
    $(container).append(row)
    $(row).append(login)
    $(login).append(inputGroup)
    $(inputGroup).append(input)
    $(inputGroup).append(inputGroupButton)
    $(inputGroupButton).append(button)

    $(".authorize").click(() => {
      const token = $(".token-input").val();
      if (token === "") {
        clearErrors();
        const error = "Token must not be blank.";
        $(container).append(createError(error))
      }
      else {
        fetchBonuses(container, token);
      }
    })
  }

  function renderBonuses(bonuses, accessToken) {
    $("#main").empty()
    renderHeader();

    for (let bonus of bonuses) {
      const row = $("<div>").addClass("row");
      const panel = $("<div>").addClass("panel panel-default");
      const giverAvatar = $("<img>").attr("src", bonus.giver.full_pic_url).addClass("avatar");
      const panelText = $("<div>").html(bonus.reason_html)
      const panelBody = $("<div>").addClass("panel-body").append(giverAvatar).append(panelText);
      $(panel).append(panelBody);
      $(row).append(panel)
      $("#feed").append(row);
    }
  }

  function renderHeader() {
    const container = $("<div>").addClass("container");
    const header = $("<h1>").addClass("header text-center").text("Bonus.ly");
    const createBonusFormContainer = $("<div>").addClass("row create-bonus-form-container");
    const createBonusForm = $("<textarea>").addClass("form-control create-bonus");
    const submitButton = $("<button>").addClass("btn btn-default submit-button").text("Create!");
    const feed = $("<div>").attr("id", "feed")
    $("#main").append(container)
    $(container).append(header)
    $(container).append(createBonusFormContainer);
    $(createBonusFormContainer).append(createBonusForm)
    $(createBonusFormContainer).append(submitButton)
    $(container).append(feed)

    $(submitButton).click(() => {
      const reason = $(createBonusForm).val()

      const data = {
        reason: reason
      }

      const accessToken = Document.cookie.split("=")[1]

      $.ajax({
        method: "POST",
        url: "https://bonus.ly/api/v1/bonuses",
        dataType: "json",
        headers: {
          "Authorization": "Bearer " + accessToken,
        },
        data: data,
        success: (data) => {
          console.log("success!");
        },
        error: () => {
          console.log("there was an error");
        }
      })
    })
  }

  function fetchBonuses(container, accessToken) {
    $.ajax({
      method: "GET",
      url: `https://bonus.ly/api/v1/bonuses`,
      dataType: "json",
      headers: {
        "Authorization": "Bearer " + accessToken,
      },
      success: (data) => {
        Document.cookie = `accessToken=${accessToken}`;
        renderBonuses(data.result)
      },
      error: () => {
        clearErrors();
        const error = "Authorization failed."
        $(container).append(createError(error));
      }
    })
  }

  function createError(error) {
    const row = $("<div>").addClass("row alert-row");
    const errorMessage = $("<div>").addClass("alert alert-danger text-center").text(error);
    $(row).append(errorMessage);
    return row;
  }

  function clearErrors() {
    $(".alert-row").remove();
  }

  renderLogin();
});
